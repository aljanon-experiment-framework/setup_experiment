#!/usr/bin/python3

# setup_experiment
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pathlib import Path

from loguru import logger
from py_command_wrapper import Command, RunArgs


def additional_setup(base_dir: Path, results_dir: Path):
    return mount_resctrl() & disable_hyperthreading()


def mount_resctrl():
    resctrl_path: Path = Path("/sys/fs/resctrl")
    logger.info(f"Mounting resctrl sysfs at {resctrl_path}")
    if str(resctrl_path) in Path("/proc/mounts").read_text():
        logger.info(f"resctrl already mounted at {resctrl_path}.")
        return True
    cmd: Command = RunArgs(
        Path("/"), ["mount", "-t", "resctrl", "resctrl", str(resctrl_path)]
    )
    if cmd.run().returncode == 0:
        logger.success("resctrl mounted successfully.")
        return True
    else:
        logger.error("Could not mount resctrl.")
        return False


def disable_hyperthreading():
    control_path: Path = Path("/sys/devices/system/cpu/smt/control")
    logger.info(f"Disabling hyperthreading using control point at {control_path}")
    bytes_written: int = control_path.write_text("off")
    text_read: str = control_path.read_text().strip()
    if bytes_written == 3 and text_read == "off":
        logger.success("Disabled hyperthreading successfully")
        return True
    else:
        logger.error(
            f"Could not disable hyperthreading. Current control value: {text_read}"
        )
        return False

#!/usr/bin/python3

# setup_experiment
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pathlib import Path
from typing import List, Tuple
from py_command_wrapper import (CommandList, RunArgs, RunArgsSaveStdout,
                                Copyfile)

class Cpuid(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["cpuid", "--version"],
                                      basedir / "cpuid_version.txt"))
        self.append(RunArgsSaveStdout(basedir, ["cpuid", "--raw"],
                                      basedir / "cpuid_dump.txt"))

class Dmidecode(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["dmidecode", "--version"],
                                      basedir / "dmidecode_version.txt"))
        self.append(RunArgs(basedir, ["dmidecode", "--dump-bin",
                                      str(basedir / "dmidecode.bin")]))

class Hwinfo(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["hwinfo", "--version"],
                                      basedir / "hwinfo_version.txt"))
        self.append(RunArgs(basedir, ["hwinfo", "--all", "--log",
                                      str(basedir / "hwinfo.txt")]))

class Lsblk(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["lsblk", "--version"],
                                      basedir / "lsblk_version.txt"))
        self.append(RunArgsSaveStdout(basedir,
                                      ["lsblk", "--all",
                                       "--output-all", "--json"],
                                      basedir / "lsblk.json"))

class LsbRelease(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["lsb_release", "--all"],
                                      basedir / "lsb_release.txt"))

class Lshw(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["lshw", "-version"],
                                      basedir / "lshw_version.txt"))
        self.append(RunArgsSaveStdout(basedir, ["lshw", "-json"],
                                      basedir / "lshw.json"))
        self.append(RunArgsSaveStdout(basedir, ["lshw", "-xml"],
                                      basedir / "lshw.xml"))
        self.append(RunArgs(basedir,
                            ["lshw", "-dump", str(basedir / "lshw.sqlite")]))

class Lsmod(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["lsmod"],
                                      basedir / "lsmod.txt"))
        

class Lspci(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["lspci", "--version"],
                                      basedir / "lspci_version.txt"))
        self.append(RunArgsSaveStdout(basedir, ["lspci", "-vvv"],
                                      basedir / "lspci_human.txt"))
        self.append(RunArgsSaveStdout(basedir, ["lspci", "-vmm"],
                                      basedir / "lspci_machine.txt"))

class Lstopo(CommandList):
    def lstopo_format(basedir, format, ext):
        return ["lstopo", "--verbose", "--logical", "--whole-system",
                "--whole-io", "--output-format", format,
                str(basedir / f"lstopo.{ext}")]

    formats: List[Tuple[str, str]] = [
            ("console", "txt"),
            ("xml", "xml"),
            # ("pdf", "pdf"),
            # ("png", "png")
    ]
        
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["lstopo", "--version"],
                                      basedir / "lstopo_version.txt"))
        for format in Lstopo.formats:
            self.append(RunArgs(basedir, Lstopo.lstopo_format(basedir,
                                                              format[0],
                                                              format[1])))

class Uname(CommandList):
    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.append(RunArgsSaveStdout(basedir, ["uname", "--version"],
                                      basedir / "uname_version.txt"))
        self.append(RunArgsSaveStdout(basedir, ["uname", "--all"],
                                      basedir / "uname.txt"))

class Proc(CommandList):
    basepath: Path = Path("/proc")
    filenames: List[str] = [
        "cmdline",
        "config.gz",
        "version"
    ]

    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        for name in Proc.filenames:
            self.append(Copyfile(basedir, Proc.basepath / name))

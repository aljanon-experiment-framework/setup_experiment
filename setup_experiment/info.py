#!usr/bin/python3

# setup_experiment
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from loguru import logger
from pathlib import Path
from py_command_wrapper import CommandList
from .command import (Cpuid, Dmidecode, Hwinfo, Lsblk, Lshw,
                      Lsmod, Lspci, Lstopo, Proc, Uname)

def write_info(info_dir: Path) -> None:
    info = Info(info_dir)
    info.run()

# TODO: add Hwinfo and Lsbrelease
class Info(CommandList):
    cls_list = [
        Cpuid,
        Dmidecode,
        Lsblk,
        Lshw,
        Lsmod,
        Lspci,
        Lstopo,
        Proc,
        Uname
    ]

    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        for cls in Info.cls_list:
            self.append(cls(basedir / cls.__name__.lower()))
